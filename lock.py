import random
import time
import os
import math
import sys
 
random.seed()
Playing = True
 
def cls():
    print("\n" * 100)
 
def progress_bar(lock, pick_location, difficulty):
    difficulty_error = 14 - (2*difficulty)
    if lock > pick_location:
        error = lock - pick_location
    else:
        error = pick_location-lock
    if error == 0:
        pass_margin = 1
    elif difficulty_error/error < 1:
        pass_margin = float(difficulty_error)/float(error)
    else:
        pass_margin = 1
    x = 0
    print(int(25*pass_margin))
    while x <= int(25*pass_margin):
        print("#"*x)
        print(" "*(25-x))
        print(": Progress (" + str(x) + "/25\r")
        time.sleep(0.04)
        x += 1
        if x == 26:
            return 1
    return 0
 
while True:
    print("Difficulty List:")
    print("Easy (1)")
    print("Medium (2)")
    print("Hard (3)")
    print("Extreame (4)")
    print("NightMare (5)")
    print("WTF (6)")
    difficulty = 0
    while difficulty == 0:
        try:
            difficulty = int(input("Please Enter Difficulty: "))
            if difficulty < 1 or difficulty > 6:
                print("Invalid Difficulty")
                difficulty = 0
        except:
            print("Invalid Difficulty")
            difficulty = 0
        time.sleep(0.1)
    picks = (16 - (difficulty*2))
    pick_history = []
    cls()
    lock = random.randint(0,180)
    while Playing:
        win = 2
        print("                    " + "Picks Remaining")
        print("      ####          " + "---------------")
        print("    ########        " + "       /       " * min(1, (int(round((picks/1)+0.5)))-1))
        print("   ####  ####       " + "       /       " * min(1, (int(round((picks/2)+0.5)))-1))
        print("  ####    ####      " + "       /       " * min(1, (int(round((picks/3)+0.5)))-1))
        print("  ###  ##  ###      " + "       /       " * min(1, (int(round((picks/4)+0.5)))-1))
        print(" ####  ##  ####     " + "       /       " * min(1, (int(round((picks/5)+0.5)))-1))
        print(" #####    #####     " + "       /       " * min(1, (int(round((picks/6)+0.5)))-1))
        print(" ######  ######     " + "       /       " * min(1, (int(round((picks/7)+0.5)))-1))
        print(" ######  ######     " + "       /       " * min(1, (int(round((picks/8)+0.5)))-1))
        print("  #####  #####      " + "       /       " * min(1, (int(round((picks/9)+0.5)))-1))
        print("  #####  #####      " + "       /       " * min(1, (int(round((picks/10)+0.5)))-1))
        print("   ##### ####       " + "       /       " * min(1, (int(round((picks/11)+0.5)))-1))
        print("    ########        " + "       /       " * min(1, (int(round((picks/12)+0.5)))-1))
        print("      ####          " + "       /       " * min(1, (int(round((picks/13)+0.5)))-1))
        print("                    " + "       /       " * min(1, (int(round((picks/14)+0.5)))-1))
        if picks == 0:
            print("Out of Picks, Game Over!")
            wait = input("Press [ENTER] to reset")
            cls()
            break
        print("Pick history: ")
        print(pick_history)
        pick_location = int(input("Enter pick angle (0-180): "))
        pick_history.append(pick_location)
        print("Picking Lock:")
        win = progress_bar(lock, pick_location, difficulty)
        if win == 0:
            wait = input("Pick broke! Press [ENTER] to try again")
        if win == 1:
            wait = input("Lock Picked. Press [ENTER] to reset")
            cls()
            break
        cls()
        picks -= 1;
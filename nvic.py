import threading
import time
import Queue
import uuid

class VectorInterruptController():
    def __init__(self):
        self.q = Queue.PriorityQueue()
    def pInterrupt(self, priority=0):
        def pInterrupt_wrap(fun):
            def inter_wrap(pin):
                id = uuid.uuid4()
                self.q.put((priority,id,fun,pin))
                while True:
                    if not self.q.empty():
                        if self.q.queue[0][1] == id:
                            res = fun(pin)
                            self.q.get()
                            return res
            return inter_wrap
        return pInterrupt_wrap

NVIC = VectorInterruptController()

@NVIC.pInterrupt(1)
def inter1(pin):
    print "Welcome to interrupt 1 on pin", pin
    print "End of interrupt 1"
    
@NVIC.pInterrupt(0)
def inter2(pin):
    print "Welcome to interrupt 2 on pin", pin
    time.sleep(3)
    print "End of interrupt 2"

def f1():
    while True:
        time.sleep(1)
        inter1(1)
        
def f2():
    while True:
        time.sleep(2)
        inter1(2)
        inter1(2)
        inter2(2)
        
def f3():
    while True:
        time.sleep(3)
        print "running"

t1 = threading.Thread(name='interruptor1', target=f1)
t2 = threading.Thread(name='interruptor2', target=f2)
t3 = threading.Thread(name='mainloop', target=f3)

t1.start()
t2.start()
t3.start()